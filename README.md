# python_file_converter

This python script file converts a xlsx to a csv file

# Usage

python xlsx_to_csv.py -i *input.xlsx" -o *output.csv"

If -o flag is not set, output file will be filename of input with csv extension.
